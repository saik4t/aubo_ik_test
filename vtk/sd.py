def countingValleys(n, s):
    step_bin = []
    for x in range(n):
        if (s[x]) == 'U':
            step_bin.append(1)
        else:
            step_bin.append(-1)
    temp_count = 0
    valley_count = 0
    for x in range(n):
        #print(temp_count)
        if (temp_count < 0):
            if(temp_count+step_bin[x]>=0):
                valley_count += 1
        temp_count += step_bin[x]

    return valley_count

s = ['U','D','D','D','U','U','U','U']
result = countingValleys(8,s)
print(result)