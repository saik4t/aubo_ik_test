import numpy as np

X = 5.0
Y = 0.0

Theta2 = np.arctan2(Y,X)

R0_6 = [[-1.0,0.0,0.0],
        [0.0,-1.0,0.0],
        [0.0,0.0,1.0]]

R0_3 = [[-np.sin(Theta2),0.0,np.cos(Theta2)],
        [np.cos(Theta2),0.0,np.sin(Theta2)],
        [0.0,1.0,0.0]]

invR0_3 = np.linalg.inv(R0_3)
R3_6 = np.dot(invR0_3,R0_6)

print('R3_6=',np.matrix(R3_6))
Theta5 = np.arccos(R3_6[2][2])
print('Theta5 = ',Theta5,'radians')
Theta6 = np.arccos(-R3_6[2][0]/np.sin(Theta5))
print('Theta6 = ',Theta6,'radians')
Theta4 = np.arccos(R3_6[1][2]/np.sin(Theta5))
print('Theta4 = ',Theta4,'radians')

def sin(angle):
        return np.sin(angle)
def cos(angle):
        return np.cos(angle)

R3_6_check = [[-sin(Theta4)*cos(Theta5)*cos(Theta6)-cos(Theta4)*sin(Theta6),sin(Theta4)*cos(Theta5)*sin(Theta6)-cos(Theta4)*cos(Theta6),-sin(Theta4)*sin(Theta5)],
              [cos(Theta4)*cos(Theta5)*cos(Theta6)-sin(Theta4)*sin(Theta6),-cos(Theta4)*cos(Theta5)*sin(Theta6)-sin(Theta4)*cos(Theta6),cos(Theta4)*sin(Theta5)],
              [-sin(Theta5)*cos(Theta6),sin(Theta5)*sin(Theta6),cos(Theta5)]]
print('R3_6_check=',np.matrix(R3_6_check))
print(np.arctan2(5,4))
print(np.arctan(5/4))