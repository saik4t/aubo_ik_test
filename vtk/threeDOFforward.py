import numpy as np
theta1 = 45
theta2 = 30
theta3 =45
a1=5
a2=4
a3=3
import math
def sin(angle):
    angle = math.radians(angle)
    return np.sin(angle)
def cos(angle):
    angle = math.radians(angle)
    return np.cos(angle)
def homogenous_trabsform_matrix(THETA, alpha, r, d):
    return [[cos(THETA), -sin(THETA) * cos(alpha), sin(THETA) * sin(alpha), r * cos(THETA)],
            [sin(THETA), cos(THETA) * cos(alpha), -cos(THETA) * sin(alpha), r * sin(THETA)],
            [0,sin(alpha),cos(alpha),d],
            [0,0,0,1]]
print(cos(90))
H0_1 = homogenous_trabsform_matrix(theta1,90,0,a1)
print("H0_1= ",np.matrix(H0_1))
H1_2 = homogenous_trabsform_matrix(theta2,0,a2,0)
print("H1_2= ",np.matrix(H1_2))
H2_3 = homogenous_trabsform_matrix(theta3,0,a3,0)
print("H2_3= ",np.matrix(H2_3))

H0_2 = np.dot(H0_1,H1_2)
H0_3 = np.dot(H0_2,H2_3)
print("H0_3= ",np.matrix(H0_3))

